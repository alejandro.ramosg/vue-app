import { mount } from '@vue/test-utils'
import App from '@/App.vue'
import RegisterForm from '@/components/RegisterForm.vue'

window.alert = () => {};

describe('RegisterForm.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(RegisterForm);
  })

  test('El usuario John Doe se añade a la lista', async () => {
    wrapper.setData(
      {firstName: "John", lastName: "Doe", email: "john.doe@example.com", password: "1234"}
    );
    
    const appWrapper = mount(App);
    const form = wrapper.find('form');

    await form.trigger('submit.prevent');
    const user = wrapper.emitted('onUserAdd')[0][0];
    appWrapper.vm.addUser(user);

    expect(appWrapper.vm.users).toContain(user);
  });

  test('La usuaria Jane Doe se añade a la lista', async () => {
    wrapper.setData(
      {firstName: "Jane", lastName: "Doe", email: "jane.doe@example.com", password: "1234"}
    );

    const appWrapper = mount(App);
    const form = wrapper.find('form');

    await form.trigger('submit.prevent');
    const user = wrapper.emitted('onUserAdd')[0][0];
    appWrapper.vm.addUser(user);

    expect(appWrapper.vm.users).toContain(user);
  });

  test('Los campos del formulario se vacian al registrar un usuario', async () => {
    const form = wrapper.find('form');

    await form.trigger('submit.prevent');

    expect(wrapper.vm.firstName).toBe("");
    expect(wrapper.vm.lastName).toBe("");
    expect(wrapper.vm.email).toBe("");
    expect(wrapper.vm.password).toBe("");
  });
});