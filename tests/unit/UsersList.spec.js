import { mount } from '@vue/test-utils'
import UsersList from '@/components/UsersList.vue'

describe('UsersList.vue', () => {
  let wrapper;
  beforeEach(() => {
    wrapper = mount(UsersList, {
      propsData: {
        users: [
          {firstName: "John", lastName: "Doe", email: "john.doe@example.com", password: "1234"},
          {firstName: "Jane", lastName: "Doe", email: "jane.doe@example.com", password: "1234"}
        ]
      }
    });
  })

  test('users es un Array', () => {
    expect(wrapper.vm.users).toBeInstanceOf(Array);
  });

  test('El usuario John Doe se renderiza en el componente', () => {
    expect(wrapper.text()).toContain("John Doe john.doe@example.com 1234");
  });

  test('La usuaria Jane Doe se renderiza en el componente', () => {
    expect(wrapper.text()).toContain("Jane Doe jane.doe@example.com 1234");
  });
});